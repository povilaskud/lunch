import datetime

from django.contrib.postgres.fields import JSONField
from django.db import models, transaction
from django.db.models import Max
from django.utils import timezone

from .exceptions import AlreadyVotedException


class BaseModel(models.Model):
    """ DRY way to use common fields across all models """
    created = models.DateTimeField(auto_now_add=True, editable=False)
    modified = models.DateTimeField(auto_now=True)

    class Meta:
        abstract = True


class Restaurant(BaseModel):
    """ Model that represents each restaurant """
    name = models.CharField(max_length=50, unique=True)
    logo = models.ImageField(
        upload_to='uploads/logos', default='uploads/no_image.jpeg')
    address = models.CharField(max_length=100, blank=True)
    phone_number = models.CharField(max_length=100, blank=True)

    def __str__(self):
        return self.name


class MenuManager(models.Manager):
    def today(self):
        """ Custom manager method to get todays menus """
        return super().get_queryset().filter(date=timezone.now())

    def today_winners(self):
        """ Custom manager method to get winning menu for today """
        most_votes = self.get_queryset().filter(
            date=timezone.now()).aggregate(Max('votes'))['votes__max']
        return self.get_queryset().filter(date=timezone.now(),
                                          votes=most_votes)


class Menu(BaseModel):
    """
    Model that represents uploaded menus.
    Menu can be uploaded as text or as an image.
    """
    restaurant = models.ForeignKey(Restaurant, on_delete=models.CASCADE)
    description_text = models.TextField(blank=True)
    description_image = models.ImageField(
        upload_to='uploads/menus', null=True)
    voters = JSONField(default=list)
    votes = models.IntegerField(default=0)
    date = models.DateField(default=timezone.now)

    objects = MenuManager()

    def __str__(self):
        return f"{self.restaurant.name} | {self.created}"

    @classmethod
    def vote(cls, id, user):
        """
        Class method that handles voting.
        Using atomic transactions to prevent race condition.
        """
        with transaction.atomic():
            menu = cls.objects.select_for_update().get(id=id)

            if user.username in menu.voters:
                raise AlreadyVotedException()

            menu.votes += 1
            menu.voters.append(user.username)
            menu.save()
            return menu
