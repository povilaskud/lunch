from rest_framework.exceptions import APIException


class AlreadyVotedException(APIException):
    status_code = 403
    default_detail = 'You already voted!.'
    default_code = 'already_voted'
