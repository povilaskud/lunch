from django.shortcuts import render
from rest_framework import viewsets
from rest_framework.response import Response
from rest_framework.authentication import BasicAuthentication
from rest_framework.permissions import IsAuthenticated

from rest_framework.mixins import CreateModelMixin, ListModelMixin

from .models import Restaurant, Menu
from .serializers import (RestaurantSerializer,
                          EmployeesSerializer,
                          MenuSerializer,
                          WinnerMenuSerializer)

from django.contrib.auth.models import User

# Test 3
from rest_framework import generics
from rest_framework.views import APIView
from rest_framework import viewsets
from rest_framework import status


class RestaurantsView(generics.ListCreateAPIView):
    """ Handles listing all restaurants and creating new ones. """
    queryset = Restaurant.objects.all()
    serializer_class = RestaurantSerializer


class EmployeesView(generics.ListCreateAPIView):
    """ Handles listing all employees. """
    queryset = User.objects.all()
    serializer_class = EmployeesSerializer


class MenusView(generics.ListCreateAPIView):
    """ Handles listing all menus and creating new ones. """
    queryset = Menu.objects.today()
    serializer_class = MenuSerializer


class VoteForMenu(APIView):
    """
    Handles listing all users in the system.
    """
    authentication_classes = (BasicAuthentication,)
    permission_classes = (IsAuthenticated,)

    def post(self, request, format=None):
        """
        Return a list of all users.
        """
        vote_for = request.POST.get('vote_for', None)
        if not vote_for:
            return Response({'detail': 'Please provide menu id.'},
                            status=status.HTTP_400_BAD_REQUEST)
        try:
            menu = Menu.objects.get(pk=vote_for)
        except Menu.DoesNotExist:
            return Response({'detail': 'Menu does not exist.'},
                            status=status.HTTP_404_NOT_FOUND)
        except Exception as e:
            return Response({'detail': f'Corrupted menu id provided. {e}'},
                            status=status.HTTP_400_BAD_REQUEST)

        voted = menu.vote(id=vote_for, user=request.user)

        return Response({'detail': 'Vote accepted!'}, status.HTTP_201_CREATED)


class GetWinner(APIView):
    """
    Handles listing of winning menu for today.
    """

    def get(self, request):
        winner_obj = Menu.objects.today_winners()
        winner = WinnerMenuSerializer(winner_obj, many=True)
        return Response(winner.data)
