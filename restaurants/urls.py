""" LUNCH API service URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URL conf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from rest_framework.routers import DefaultRouter
from django.conf.urls import include, re_path
from django.urls import path
from .views import (RestaurantsView,
                    EmployeesView,
                    MenusView,
                    VoteForMenu,
                    GetWinner)

app_name = 'lunch'

urlpatterns = [
    path('restaurants/', RestaurantsView.as_view(), name='restaurants'),
    path('employees/', EmployeesView.as_view(), name='employees'),
    path('menus/', MenusView.as_view(), name='menus'),
    path('menus/vote/', VoteForMenu.as_view(), name='vote'),
    path('menus/winner/', GetWinner.as_view(), name='winner'),
]
