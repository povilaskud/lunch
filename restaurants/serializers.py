from django.contrib.auth.models import User

from .models import Restaurant, Menu
from rest_framework import serializers
from rest_framework.validators import UniqueValidator


class RestaurantSerializer(serializers.ModelSerializer):
    name = serializers.CharField(max_length=50)
    logo = serializers.ImageField(allow_empty_file=True, required=False)
    address = serializers.CharField(max_length=500, required=False)
    phone_number = serializers.CharField(max_length=10, required=False)

    class Meta:
        model = Restaurant
        fields = ('id', 'name', 'logo', 'address', 'phone_number', 'created')


class EmployeesSerializer(serializers.ModelSerializer):
    DUPLICATE_USER = "User with this username already exist."
    username = serializers.CharField(max_length=50, required=True, validators=[
                                     UniqueValidator(queryset=User.objects.all(),
                                                     message=DUPLICATE_USER)])
    password = serializers.CharField(max_length=50, write_only=True)

    class Meta:
        model = User
        fields = ('id', 'username', 'password')

    def create(Self, validated_data):
        try:
            return User.objects.create_user(**validated_data)
        except Exception as e:
            raise serializers.ValidationError()


class MenuSerializer(serializers.ModelSerializer):
    description_text = serializers.CharField(max_length=500, required=False)
    description_image = serializers.ImageField(
        allow_empty_file=True, required=False)
    date = serializers.DateField()

    class Meta:
        model = Menu
        fields = ('id', 'restaurant', 'description_text',
                  'description_image', 'votes', 'date')

    def create(self, validated_data):
        description_text = validated_data.get('description_text', None)
        description_image = validated_data.get('description_image', None)
        if not description_text and not description_image:
            raise serializers.ValidationError(
                'Please provide either text or image of your menu.')
        return Menu.objects.create(**validated_data)


class WinnerMenuSerializer(serializers.ModelSerializer):
    restaurant = RestaurantSerializer(read_only=True)

    class Meta:
        model = Menu
        fields = ('id', 'restaurant', 'description_text',
                  'description_image', 'votes', 'date')
