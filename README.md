# LUNCH API

Check [API documentation](https://documenter.getpostman.com/view/1201818/S1LySmw4#intro).

## 1. Want to start quickly?

`docker-compose up --build`

## 2. Long way..

- Create virtualenv
- Install pip requirements > `pip install -r requirements.pip`
- Create PostgeSQL database
- Edit `settings.py` with DB details
- Run migrations > `python manage.py migrate`
- Load fixtures > `python manage.py loaddata fixtures/db.json`
- Start dev server > `python manage.py runserver`

## 3. Using Postman 2?

Grab `CORNER.postman_collection.json` from `etc/` folder and import it to your Postman 2. You will have a collection with all API endpoints for testing.

## 4. How to run tests?

**Docker container:**

You will need to connect to the container `docker-compose exec web bash` and then run `pytest`

**Local development:**

Just run command `pytest` or
If you use VSCode editor I suggest trying this cool test explorer
https://github.com/kondratyev-nv/vscode-python-test-adapter

# Happy code reviewing! ;)
