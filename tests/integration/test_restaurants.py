import json

from django.test import TestCase
from django.urls import reverse
from django.contrib.auth import get_user_model

from restaurants.models import Restaurant

User = get_user_model()


class RestaurantTestCase(TestCase):

    def setUp(self):
        self.url = reverse('lunch:restaurants')
        self.restaurant_a = {
            "name": "Casa Della Pasta",
            "address": "Random 1A",
            "phone_number": "868888888"
        }
        self.restaurant = Restaurant.objects.create(**self.restaurant_a)

    def test_can_create_new_restaurant(self):
        """ Test to verify that you can create new restaurant """
        restaurant_b = {
            "name": "Pjazz",
            "address": "Random 2A",
            "phone_number": "868888888"
        }
        response = self.client.post(self.url, restaurant_b)
        self.assertEqual(201, response.status_code)

    def test_list_all_restaurants(self):
        """ Test to verify that you can list all restaurants """
        response = self.client.get(self.url)
        self.assertEqual(200, response.status_code)
        self.assertTrue(self.restaurant_a['name'] in str(response.content))
