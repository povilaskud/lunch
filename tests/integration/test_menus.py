import json
import base64

from django.test import TestCase
from django.urls import reverse
from django.contrib.auth import get_user_model
from django.utils import timezone

from restaurants.models import Menu, Restaurant

User = get_user_model()


class MenuTestCase(TestCase):

    def setUp(self):
        self.url = reverse('lunch:menus')

        self.restaurant_a_dict = {
            "name": "Casa Della Pasta",
            "address": "Random 1A",
            "phone_number": "868888888"
        }
        self.restaurant_a = Restaurant.objects.create(**self.restaurant_a_dict)
        self.restaurant_b_dict = {
            "name": "Bonjorno",
            "address": "Random 2A",
            "phone_number": "868888888"
        }
        self.restaurant_b = Restaurant.objects.create(**self.restaurant_b_dict)

        self.menu_a_dict = {
            "restaurant": self.restaurant_a,
            "description_text": "Random dish restaurant_a",
            "date": timezone.now().strftime("%Y-%m-%d")
        }
        self.menu_a = Menu.objects.create(**self.menu_a_dict)

        self.menu_b_dict = {
            "restaurant": self.restaurant_b,
            "description_text": "Random dish restaurant_b",
            "date": timezone.now().strftime("%Y-%m-%d")
        }
        self.menu_b = Menu.objects.create(**self.menu_b_dict)

        self.user_a = {
            "username": "User1",
            "password": "123123",
        }
        self.user = User.objects.create(username=self.user_a['username'])
        self.user.set_password(self.user_a['password'])
        self.user.save()

    def test_can_create_new_menu(self):
        """ Test to verify that you can create new menu """
        menu_a = {
            "restaurant": self.restaurant_a.id,
            "description_text": "Random dish text",
            "date": timezone.now().strftime("%Y-%m-%d")
        }
        response = self.client.post(self.url, menu_a)
        self.assertEqual(201, response.status_code)

    def test_list_today_menus(self):
        """ Test to verify that you can list menus for today """
        response = self.client.get(self.url)
        self.assertEqual(200, response.status_code)
        self.assertTrue(self.menu_a_dict['description_text']
                        in str(response.content))

    def test_menu_can_be_voted(self):
        """ Test to verify that you can vote for menu """
        headers = {
            'HTTP_AUTHORIZATION': 'Basic ' + base64.b64encode(b'User1:123123').decode("ascii")
        }
        data = {
            'vote_for': self.menu_a.id
        }
        url = reverse('lunch:vote')
        response = self.client.post(url, data, **headers)
        self.assertEqual(201, response.status_code)

    def test_cannot_vote_twice_for_same_menu(self):
        """ Test to verify that you cannot vote twice for same menu """

        headers = {
            'HTTP_AUTHORIZATION': 'Basic ' + base64.b64encode(b'User1:123123').decode("ascii")
        }
        data = {
            'vote_for': self.menu_a.id
        }
        url = reverse('lunch:vote')
        response_a = self.client.post(url, data, **headers)
        self.assertEqual(201, response_a.status_code)
        response_b = self.client.post(url, data, **headers)
        self.assertEqual(403, response_b.status_code)

    def test_get_winners_for_today(self):
        """ Test to verify that you can get list most voted menus for today """
        url = reverse('lunch:winner')
        headers = {
            'HTTP_AUTHORIZATION': 'Basic ' + base64.b64encode(b'User1:123123').decode("ascii")
        }
        data = {
            'vote_for': self.menu_a.id
        }
        url_vote = reverse('lunch:vote')
        response_a = self.client.post(url_vote, data, **headers)
        self.assertEqual(201, response_a.status_code)

        url_winner = reverse('lunch:winner')
        response = self.client.get(url_winner)
        self.assertEqual(200, response.status_code)
        self.assertTrue(self.menu_a_dict['description_text']
                        in str(response.content))
