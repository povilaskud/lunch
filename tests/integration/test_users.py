import json

from django.test import TestCase
from django.urls import reverse
from django.contrib.auth import get_user_model

User = get_user_model()


class UserTestCase(TestCase):

    def setUp(self):
        self.url = reverse('lunch:employees')
        self.user_data = {
            "username": "User1",
            "password": "123123",
        }
        self.user = User.objects.create(username=self.user_data['username'])
        self.user.set_password(self.user_data['password'])
        self.user.save()

    def test_employee_registration(self):
        """
        Test to verify that a post call with user valid data
        """
        user_data = {
            "username": "User2",
            "password": "123123",
        }
        response = self.client.post(self.url, user_data)
        self.assertEqual(201, response.status_code)

    def test_cant_register_same_username(self):
        """ Test to verify that users can't register with dublicate username """
        response = self.client.post(self.url, self.user_data)
        self.assertEqual(400, response.status_code)

    def test_get_users_list(self):
        """ Test to verify that you can get a list of users """
        response = self.client.get(self.url)
        self.assertEqual(200, response.status_code)
        self.assertTrue(self.user_data['username'] in str(response.content))
