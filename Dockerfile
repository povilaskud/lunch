FROM python:3.6.8
LABEL version="0.1.0"

COPY . /home/lunchproject
WORKDIR /home/lunchproject

RUN pip install -r requirements.pip

